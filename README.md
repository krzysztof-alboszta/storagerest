REST API 
=========================


Installation
============


```
composer require skladiste/skladiste-rest-bundle symfony/serializer
```

`symfony/serializer` component is required by FOSRestBundle (serializer provided by `JMSSerializerBundle` would also work)


Configure routing
=================

import routing for API controllers

add below to `config/routes.yaml`

```yaml
skladiste_rest:
  resource: "@SkladisteRestBundle/Resources/config/routing/all.xml"

```

Configure Entity
================

model `Product` is mapped as a super class to provide abstract mapping required for bundle, extend it with own entity mapped to desired table and 
with defined identifier

```
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Skladiste\RestBundle\Model\Product as ProductModel;

/**
 * @ORM\Entity
 * @ORM\Table(name="product")
 */
class Product extends ProductModel
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

}
```

Routes
======

by default without providing expecting format output is sent as a JSON

- `GET /products.{format}`  e.g. `GET /prodcts.json` return all products in DB
- `GET /products/{id}.{format}`  e.g. `GET /products/2.xml` return data of product ID:2
- `POST /products.{format}`  e.g. `POST /products.json {"product": {"name": "Sample product", "amount": 25}}` creates new _Sample Product_
- `PUT /products/{id}.{format} e.g. `PUT /products/12.json {"product": {"name": "New name", "amount": 0}}` updates product ID:12
- `DELETE /products/{id}.{format} e.g. `DELETE /products/1` deletes product ID:1

additional predefined collections are returned with `GET /products.{format}` call with `?collection={collection}` query
- `?collection=available` - only available products
- `?collection=unavailable` - only unavailable products
- `?collection=safe-available` - only products with at least 5 items in stock