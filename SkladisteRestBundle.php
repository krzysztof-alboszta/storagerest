<?php

namespace Skladiste\RestBundle;

use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SkladisteRestBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $this->registerDoctrineMappings($container);
    }

    public function registerDoctrineMappings(ContainerBuilder $container): void
    {
        $mappings = [
            realpath(__DIR__ . '/Resources/config/doctrine') => 'Skladiste\RestBundle\Model',
        ];

        $container->addCompilerPass(DoctrineOrmMappingsPass::createXmlMappingDriver($mappings));

    }
}