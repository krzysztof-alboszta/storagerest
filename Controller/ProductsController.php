<?php

namespace Skladiste\RestBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Skladiste\RestBundle\Form\ProductDTO;
use Skladiste\RestBundle\Form\ProductType;
use Skladiste\RestBundle\Model\ProductManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductsController extends FOSRestController
{

    private $productManager;

    public function getProductsAction(Request $request): Response
    {
        $collection = $request->get('collection');
        switch ($collection) {
            case 'available':
                $products = $this->getProductManager()->findAvailableProducts();

                break;
            case 'unavailable':
                $products = $this->getProductManager()->findNotAvailableProducts();

                break;
            case 'safe-available':
                $products = $this->getProductManager()->findSafeAvailableProducts();

                break;
            default:
                $products = $this->getProductManager()->findProducts();

        }
        $view = $this->view($products, Response::HTTP_OK);

        return $this->handleView($view);
    }

    public function getProductAction($id): Response
    {
        $view = $this->view(null, Response::HTTP_NOT_FOUND);
        $product = $this->getProductManager()->findProduct($id);
        if ($product) {
            $view->setData($product)
                ->setStatusCode(Response::HTTP_OK);
        }

        return $this->handleView($view);
    }

    public function postProductAction(Request $request): Response
    {
        $form = $this->createForm(ProductType::class, new ProductDTO());
        $form->handleRequest($request);
        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->handleView(
                $this->view(['errors' => $form->getErrors()], Response::HTTP_UNPROCESSABLE_ENTITY)
            );
        }

        $product = $this->getProductManager()->create($form->getData());
        $this->getProductManager()->update($product, true);

        return $this->handleView($this->view([], Response::HTTP_CREATED));
    }

    public function putProductAction($id, Request $request): Response
    {
        $view = $this->view(null, Response::HTTP_NOT_FOUND);
        $product = $this->getProductManager()->findProduct($id);
        if (!$product) {
            return $this->handleView($view);
        }
        $form = $this->createForm(ProductType::class, new ProductDTO(), ['method' => 'PUT']);
        $form->handleRequest($request);

        $data = $form->getData();
        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->handleView(
                $this->view(['errors' => $form->getErrors(), 'data' => $data],
                    Response::HTTP_UNPROCESSABLE_ENTITY)
            );
        }

        $product->updateAmount($data->getAmount());
        $product->updateName($data->getName());
        $this->getProductManager()->update($product, true);

        return $this->handleView($this->view([], Response::HTTP_NO_CONTENT));
    }

    public function deleteProductAction($id)
    {
        $view = $this->view(null, Response::HTTP_NO_CONTENT);
        $product = $this->getProductManager()->findProduct($id);
        if (!$product) {
            $view->setStatusCode(Response::HTTP_NOT_FOUND);
        }

        $this->getProductManager()->removeProduct($product);
        return $this->handleView($view);

    }


    private function getProductManager(): ProductManagerInterface
    {
        if (!$this->productManager) {
            $this->productManager = $this->get(\Skladiste\RestBundle\Doctrine\ProductManager::class);
        }

        return $this->productManager;
    }
}