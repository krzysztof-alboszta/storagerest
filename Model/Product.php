<?php

namespace Skladiste\RestBundle\Model;


class Product implements \JsonSerializable
{

    public const SAFE_AVAILABILITY_MINIMUM = 5;
    protected $id;
    private $name;
    private $amount;

    public function __construct(string $name, int $amount)
    {
        $this->name = $name;
        $this->amount = $amount;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function updateName(string $newName): void
    {
        $this->name = $newName;
    }

    public function updateAmount(int $newAmount): void
    {
        $this->amount = $newAmount;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'amount' => $this->amount,
        ];
    }
}