<?php

namespace Skladiste\RestBundle\Model;

use Skladiste\RestBundle\Form\ProductDTO;

interface ProductManagerInterface
{
    public function findProducts(): array;

    public function findAvailableProducts(): array;

    public function findSafeAvailableProducts(): array;

    public function findNotAvailableProducts(): array;

    public function findProduct(int $id): ?Product;

    public function removeProduct(Product $product);

    public function create(ProductDTO $dto): Product;

    public function update(Product $product, bool $doSave = false): void;
}