<?php

namespace Skladiste\RestBundle\Doctrine;


use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Skladiste\RestBundle\Form\ProductDTO;
use Skladiste\RestBundle\Model\Product;
use Skladiste\RestBundle\Model\ProductManagerInterface;

final class ProductManager implements ProductManagerInterface
{
    private $entityManager;
    private $entityClass;

    public function __construct(EntityManagerInterface $entityManager, string $entityClass)
    {
        $this->entityManager = $entityManager;
        $this->entityClass = $entityClass;
    }

    public function findProducts(): array
    {
        return $this->getRepository()->findAll();
    }

    public function findAvailableProducts(): array
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()
                ->gt('amount', 0)
            );

        return $this->getRepository()->matching($criteria)->toArray();
    }

    public function findSafeAvailableProducts(): array
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()
                ->gt('amount', Product::SAFE_AVAILABILITY_MINIMUM)
            );

        return $this->getRepository()->matching($criteria)->toArray();
    }

    public function findNotAvailableProducts(): array
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()
                ->eq('amount', 0)
            );

        return $this->getRepository()->matching($criteria)->toArray();
    }

    public function findProduct(int $id): ?Product
    {
        return $this->getRepository()->find($id);
    }

    public function removeProduct(Product $product): void
    {
        $this->entityManager->remove($product);
        $this->entityManager->flush();
    }

    private function getRepository(): EntityRepository
    {
        return $this->entityManager->getRepository($this->entityClass);
    }

    public function create(ProductDTO $dto): Product
    {
        return new $this->entityClass($dto->getName(), $dto->getAmount());
    }

    public function update(Product $product, bool $doSave = false): void
    {
        $this->entityManager->persist($product);

        if ($doSave) {
            $this->entityManager->flush($product);
        }
    }


}